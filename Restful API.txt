﻿* RESTful API là một tiêu chuẩn dùng trong việc thết kế các thiết kế API cho các ứng dụng web để quản lý các resource.
* RESTful là một trong những kiểu thiết kế API được sử dụng phổ biến nhất ngày nay.
Trọng tâm của REST quy định cách sử dụng các HTTP method (như GET, POST, PUT, DELETE...) và cách định dạng các URL cho ứng dụng web để quản các resource. 
Ví dụ với một trang blog để quản lý các bài viết chúng ta có các URL đi với HTTP method như sau:
- URL tạo bài viết: http://my-blog.xyz/posts. Tương ứng với HTTP method là POST
- URL đọc bài viết với ID là 123: http://my-blog.xyz/posts/123. Tương ứng với HTTP method là GET
- URL cập nhật bài viết với ID là 123: http://my-blog.xyz/posts/123. Tương ứng với HTTP method là PUT
- URL xoá bài viết với ID là 123: http://my-blog.xyz/posts/123. Tương ứng với HTTP method là DELETE
Với các ứng dụng web được thiết kế sử dụng RESTful, lập trình viên có thể dễ dàng biết được URL và HTTP method để quản lý một resource. 
Bạn cũng cần lưu ý bản thân RESTful không quy định logic code ứng dụng và RESTful cũng không giới hạn bởi ngôn ngữ lập trình ứng dụng. 
Bất kỳ ngôn ngữ lập trình (hoặc framework) nào cũng có thể áp dụng RESTful trong việc thiết kế API cho ứng dụng web.

* API (Application Programming Interface- giao diện lập trình ứng dụng) là một giao diện mà một hệ thống máy tính hay ứng dụng cung cấp để cho phép các 
yêu cầu dịch vụ có thể được tạo ra từ các chương trình máy tính khác, và/hoặc cho phép dữ liệu có thể được trao đổi qua lại giữa chúng. Chúng ta có thể viết
các API chuẩn và cho phép tất cả các platform gửi request tới API đó mà không phải thay đổi code quá nhiều.

- API làm việc: Phía người dùng gửi request, API sẽ gửi lại response là liệu có thể làm được cái người dùng muốn hay ko. Và API được xây dựng trên 
2 thành phần chính: Request và Response.

* Request: Một cái request đúng chuẩn cần có 4 thứ:
- URL: là 1 cái địa chỉ duy nhất cho 1 request, thường là đường dẫn tới một hàm xử lí logic.
- Method: là cái hành động người dùng muốn tác động lên dữ liệu. Có 4 loại Method hay được dùng và rất quen thuộc là: GET, POST, PUT, DELETE.
- Headers: nơi chứa các thông tin cần thiết của 1 request nhưng người dùng không biết có sự tồn tại của nó. Ví dụ: độ dài của request body, thời gian gửi request, 
loại thiết bị đang sử dụng, ...
- Body: nơi chứa thông tin mà người dùng sẽ điền. Giả sử bạn đặt 1 cái bánh pizza, thì thông tin ở phần body sẽ là: Loại bánh pizza, kích cỡ, số lượng đặt.

* Response: Sau khi nhận được request từ phía người dùng, API sẽ xử lý cái request đó và gửi ngược lại cho người dùng 1 cái response. 
Cấu trúc của 1 response tương đối giống phần request nhưng Status code sẽ thay thế cho URL và Method. Tóm lại, nó có cầu trúc 3 phần:
- Status code: là những con số có 3 chữ số và có duy nhất 1 ý nghĩa, ví dụ như vài lỗi thần thánh quen thuộc “404 Not Found” hoặc “503 Service Unavailable”. 
Bạn có thể tìm hiểu cụ thể từng loại mã và ý nghĩa của nó tại đây.
- Headers: giống với headers trong request.
- Body: tương đối giống với trong request.
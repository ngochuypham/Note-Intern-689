﻿PagingAndSortingRepository: Kiểu trả về là Page<T>:

  Iterable<T> findAll(Sort sort);

  Page<T> findAll(Pageable pageable); Truyền vào PageRequest(int pageNum, int size, Direction.ASC/DESC, "fieldToOrder");

  Page<T>.getContent trả về List<T>
﻿* JPA hay Java Persistence API là một đặc tả Java cho việc ánh xạ giữa các đối tượng Java tới cơ sở dữ liệu quan hệ sử dụng 
công nghệ phổ biến là ORM (Object Relationship Mapping). Thông qua JPA, các ORM framework có thể giúp lập trình viên thao tác 
cơ sở dữ liệu một cách nhanh chóng và trực quan hơn.

* JPA hỗ trợ đầy đủ các mối quan hệ mà RDBMS cung cấp đó là:
- OneToMany: một - nhiều
- ManyToOne: nhiều - một
- ManyToMany: nhiều - nhiều
- OneToOne: một - một

* Để ánh xạ đối tượng Java và cơ sở dữ liệu thì JPA cung cấp cho chúng ta hai cách:
- Ánh xạ sử dụng xml file
- Ánh xạ sử dụng annotation

* Một class Java chỉ được xem là một entity ánh xạ với database khi thoả mãn các điều kiện sau:
- Được đánh dấu bởi @Entity annotation
- Phải implements Serializable interface
- Có một constructor không tham số
- Getter và setter đầy đủ cho huhu